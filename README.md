# Stakeholder data for the klimamonitor

This repository contains scripts to generate data for the stakeholder 
view. We use two datasets:

1. DWD NKDZ: interpolierter Temperaturdatensatz des Nationalen Klimadatenzentrums (NKDZ) des Deutschen Wetterdienstes (DWD)
   
   used for the calculation of the mean temperature difference

2. CRU TS3.23:

   used for the calculation of the precipitation difference


## Installation
Necessary dependencies are `xarray`, `distributed` and `jupyter`. Please
setup a conda environment via `conda env create -f environment.yml`.
The files in the [`data`](./data) folder have been generated with the
`environment_prod.yml` environment.
